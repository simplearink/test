package com.smef.utils;

/**
 * Util class StringParser provides:
 * parsing from String to int, long, String arrays.
 *
 * Separator is defined by constructor
 * If no is provided, default is used
 *
 * Usages: allows treating multiple parameters as a String in Jersey
 */
public class StringParser {
    private static final String DEFAULT_SEPARATOR = ",";
    private String separator;


    public StringParser() {
        this.separator = DEFAULT_SEPARATOR;
    }

    public StringParser(String separator) {
        this.separator = separator;
    }

    public String getSeparator() {
        return separator;
    }

    /**
     * The method is a wrapper of {@code String.split()} method
     * It allows to add some actions to do with a string before splitting
     * i.e. - reduce spaces
     * @param string
     * @param separator
     * @return
     */
    private static String[] separate(String string, String separator) {
        String result = string;

        result = result.replaceAll(" ", "");

        return result.split(separator);
    }

    /**
     * Converter
     * @param string
     * @return int array
     */
    public int[] ToInts(String string) {
        String[] strings = separate(string, getSeparator());
        int N = strings.length;
        int[] result = new int[N];
        for (int i = 0; i < N; i++)
            result[i] = Integer.parseInt(strings[i]);
        return result;
    }

    /**
     * Converter
     * @param string
     * @return long array
     */
    public long[] ToLongs(String string) {
        String[] strings = separate(string, getSeparator());
        int N = strings.length;
        long[] result = new long[N];
        for (int i = 0; i < N; i++)
            result[i] = Long.parseLong(strings[i]);
        return result;
    }

    /**
     * Converter
     * @param string
     * @return String array
     */
    public String[] ToStrings(String string) {
        return separate(string, getSeparator());
    }

}
