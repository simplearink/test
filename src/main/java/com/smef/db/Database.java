package com.smef.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {

	private Connection connection = null;

	public Database() {

		String url = "jdbc:postgresql://smef_postgres_1/postgres?user=postgres&password=1&ssl=false";
		// String user = "postgres";
		// String password = "1";
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(url);// , user, password);
		} catch (Exception e) {
			System.out.println("DB error: " + e);
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connnection) {
		this.connection = connnection;
	}

	public ResultSet getResultSetForQuery(String sql) throws SQLException {
		Statement st = getConnection().createStatement();
		ResultSet rs = st.executeQuery(sql);
		return rs;

	}
	
	
}
