package com.smef.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.smef.entity.Region;
import com.smef.entity.Smef;

public class SmefDB extends RegionDB {

	public List<Smef> getSmefsByIDs(int[] id) {
		List<Smef> smefs = new ArrayList<>();
		for (int i : id) {
			List<Smef> current = getSmefsByParametres(i, null, 0);
			smefs.addAll(current);
		}
		return smefs;
	}

	public List<Smef> getSmefsByCommitIDs(String[] commitID) {
		List<Smef> smefs = new ArrayList<>();
		for (String i : commitID) {
			List<Smef> current = getSmefsByParametres(0, i, 0);
			smefs.addAll(current);
		}
		return smefs;
	}

	public List<Smef> getSmefsByCommitTSs(long[] commitTS) {
		List<Smef> smefs = new ArrayList<>();
		for (long i : commitTS) {
			List<Smef> current = getSmefsByParametres(0, null, i);
			smefs.addAll(current);
		}
		return smefs;
	}

	public Smef postSmef(Smef smef) {
		String sql = "INSERT INTO region VALUES (?,?,?,?,?,?,?)";
		for (Region r : smef.getRegions()) {
			try {

				PreparedStatement st = getConnection().prepareStatement(sql);
				st.setInt(1, r.getId());
				st.setInt(2, r.getParentId());
				st.setTimestamp(3, new Timestamp(r.getExctractionTimeStamp()));
				st.setInt(4, r.getLanguage());
				st.setInt(5, r.getType());
				st.setString(6, r.getName());
				st.setInt(7, r.getProjectId());
				st.executeUpdate();

			} catch (SQLException e) {
				System.out.println("DB error: " + e);
			}
		}
		return smef;
	}

	public List<Smef> getSmefsByParametres(int id, String commitID, long commitTS) {
		List<Smef> smefs = new ArrayList<>();
		String sql = "SELECT * FROM project";
		if (id != 0 || commitID != null || commitTS != 0) {
			sql += " WHERE";
			if (id != 0)
				sql += " id=" + id + " AND";
			if (commitID != null)
				sql += " commit_id=" + commitID + " AND";
			if (commitTS != 0)
				sql += " commit_ts=" + commitTS + " AND";
			sql = sql.substring(0, sql.length() - 3);
		}

		try {
			Statement st = getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Smef smef = new Smef();
				smef.getProject().setId(rs.getInt(1));
				smef.getProject().setExtractionTimestamp(rs.getTimestamp(2).getTime());
				smef.getProject().setUrl(rs.getString(3));
				smef.getProject().setName(rs.getString(4));
				smef.setRegions(getRegionsByParametres(0, 0, 0, null, smef.getProject().getId(),
						smef.getProject().getCommitID(), smef.getProject().getCommitTS()));
				smefs.add(smef);
			}
		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}
		return smefs;
	}
}
