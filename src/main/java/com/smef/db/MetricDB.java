package com.smef.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.smef.entity.MetricStore;

public class MetricDB extends Database {

	public MetricDB() {
		super();
	}

	public List<MetricStore> getMetricStore() {
		List<MetricStore> metricStore = new ArrayList<>();
		String sql = "SELECT * FROM metric_store";
		try {
			Statement st = getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				MetricStore ms = new MetricStore();
				ms.setId(rs.getInt(1));
				ms.setRegionId(rs.getInt(2));
				ms.setMetricCode(rs.getInt(3));
				ms.setValue(rs.getDouble(4));

				metricStore.add(ms);
			}
		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}
		return metricStore;
	}

	public MetricStore getMetricStoreById(int id) {
		MetricStore ms = new MetricStore();
		String sql = "SELECT * FROM metric_store WHERE id=" + id;
		try {
			Statement st = getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) {
				ms.setId(rs.getInt(1));
				ms.setRegionId(rs.getInt(2));
				ms.setMetricCode(rs.getInt(3));
				ms.setValue(rs.getDouble(4));
			}
		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}
		return ms;
		// TODO: maybe better to handle when there is no element with this id
	}

	public List<MetricStore> getMetricStoreByRegionId(int id) {
		List<MetricStore> metricStore = new ArrayList<>();
		String sql = "SELECT * FROM metric_store WHERE region_id=" + id;

		try {
			Statement st = getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				MetricStore ms = new MetricStore();
				ms.setId(rs.getInt(1));
				ms.setRegionId(rs.getInt(2));
				ms.setMetricCode(rs.getInt(3));
				ms.setValue(rs.getDouble(4));

				metricStore.add(ms);

			}
		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}
		return metricStore;
	}

	public List<MetricStore> getMetricStoreByMetricCode(int code) {
		List<MetricStore> metricStore = new ArrayList<>();
		String sql = "SELECT * FROM metric_store WHERE metric_code=" + code;
		try {
			Statement st = getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				MetricStore ms = new MetricStore();
				ms.setId(rs.getInt(1));
				ms.setRegionId(rs.getInt(2));
				ms.setMetricCode(rs.getInt(3));
				ms.setValue(rs.getDouble(4));

				metricStore.add(ms);

			}
		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}
		return metricStore;
	}

	public List<MetricStore> getMetricStoreByParametres(int id, int code) {
		List<MetricStore> metricStore = new ArrayList<>();
		String sql = "SELECT * FROM metric_store WHERE region_id=" + id + " AND metric_code=" + code;
		System.out.println(sql);
		try {
			Statement st = getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				MetricStore ms = new MetricStore();
				ms.setId(rs.getInt(1));
				ms.setRegionId(rs.getInt(2));
				ms.setMetricCode(rs.getInt(3));
				ms.setValue(rs.getDouble(4));

				metricStore.add(ms);

			}
		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}
		return metricStore;
	}

	public MetricStore postMetricToStore(MetricStore m) {

		String sql = "INSERT INTO metric_store VALUES (?,?,?,?)";
		try {
			PreparedStatement st = getConnection().prepareStatement(sql);
			st.setInt(1, m.getId());
			st.setInt(2, m.getRegionId());
			st.setInt(3, m.getMetricCode());
			st.setDouble(4, m.getValue());
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println("DB error: " + e);
		}

		return m;
	}
}
