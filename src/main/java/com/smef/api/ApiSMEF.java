package com.smef.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.smef.db.SmefDB;
import com.smef.entity.Smef;
import com.smef.utils.StringParser;

@Path("smef")
public class ApiSMEF {
    SmefDB db = new SmefDB();
    StringParser parser = new StringParser(",");

    @POST
    @Consumes({MediaType.TEXT_XML, MediaType.APPLICATION_JSON})
    public Smef postSmef(Smef smef) {
        System.out.println("/smef - post smef called");
        return db.postSmef(smef);
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Smef> getSmefsByParametres(@QueryParam("id") int id, @QueryParam("commitID") String commitID,
                                           @QueryParam("CommitTS") long commitTS) {
        return db.getSmefsByParametres(id, commitID, commitTS);

    }


    @GET
    @Path("id")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Smef> getSmefsByIds(@QueryParam("id") String ids) {
        System.out.println("/smef/id/{id}");
        int[] id = parser.ToInts(ids);
        return db.getSmefsByIDs(id);
    }

    @GET
    @Path("commitID")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Smef> getSmefsByCommitIDs(@QueryParam("id") String commitIDs) {
        System.out.println("/smef/commitID/{commitID}");
        String[] commitID = parser.ToStrings(commitIDs);
        return db.getSmefsByCommitIDs(commitID);
    }

    @GET
    @Path("commitTS")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Smef> getSmefsByCommitTSs(@QueryParam("ts") String commitTSs) {
        System.out.println("/smef/commitTS/{commitTS}");
        long[] commitTS = parser.ToLongs(commitTSs);
        return db.getSmefsByCommitTSs(commitTS);
    }


}
