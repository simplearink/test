package com.smef.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.smef.db.MetricDB;
import com.smef.entity.MetricStore;
import com.smef.utils.StringParser;

@Path("metrics")
public class ApiMetrics {
	MetricDB db = new MetricDB();
	StringParser parser = new StringParser(",");

	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<MetricStore> getMetricStore() {
		System.out.println("/metrics called");
		return db.getMetricStore();
	}

	@GET
	@Path("id")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public MetricStore getMetricStoresByIDs(@QueryParam("id") int id) {
		return db.getMetricStoresByIDs(id);

	}

	@GET
	@Path("query")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<MetricStore> getMetricStoreByParametres(@QueryParam("regionId") int id,
			@QueryParam("metricCode") int code) {
		if (id == 0)
			return db.getMetricStoreByMetricCode(code);
		else if (code == 0)
			return db.getMetricStoreByRegionId(id);
		else
			return db.getMetricStoreByParametres(id, code);
	}

	@GET
	@Path("regionId/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<MetricStore> getMetricStoreByRegionId(@PathParam("id") int id) {

		return db.getMetricStoreByRegionId(id);

	}

	@GET
	@Path("metricCode/{code}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<MetricStore> getMetricStoreByMetricCode(@PathParam("code") int code) {
		return db.getMetricStoreByMetricCode(code);

	}

	@POST
	@Path("metric")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public MetricStore postMetricToStore(MetricStore m) {

		return db.postMetricToStore(m);
	}

}
