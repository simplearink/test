package com.smef.api;

import java.sql.Timestamp;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.smef.db.RegionDB;
import com.smef.entity.Region;
import com.smef.utils.StringParser;

@Path("regions")
public class ApiRegions {

    RegionDB db = new RegionDB();
    StringParser parser = new StringParser(",");

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Region> getRegions() {
        System.out.println("/regions called");

        return db.getRegions();
    }

    @GET
    @Path("id")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Region> getRegionsByIDs(@QueryParam("id") String ids) {
        int[] id = parser.ToInts(ids);
        return db.getRegionsByIDs(id);

    }

    @GET
    @Path("name")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Region> getRegionsByNames(@QueryParam("name") String names) {
        String[] name = parser.ToStrings(names);
        return db.getRegionsByNames(name);

    }

    @GET
    @Path("type")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Region> getRegionsByTypes(@QueryParam("type") String types) {
        int[] type = parser.ToInts(types);
        return db.getRegionsByTypes(type);

    }

    @GET
    @Path("commit_ts")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Region> getRegionsByCommitTSs(@QueryParam("ts") String tss) {
        long[] ts = parser.ToLongs(tss);
        return db.getRegionsByCommitTSs(ts);
    }

    @GET
    @Path("commit_id")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Region> getRegionsByCommitIDs(@QueryParam("id") String ids) {
        String[] id = parser.ToStrings(ids);
        return db.getRegionsByCommitIDs(id);
    }

    @GET
    @Path("query")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Region> getRegionsByParametres(@QueryParam("extractionTS") long extractionTS,
                                               @QueryParam("language") int language, @QueryParam("type") int type, @QueryParam("name") String name,
                                               @QueryParam("projectID") int projectID, @QueryParam("commitID") String commitID,
                                               @QueryParam("commitTS") long commitTS) {
        return db.getRegionsByParametres(extractionTS, language, type, name, projectID, commitID, commitTS);

    }

    @GET
    @Path("queryWithParentId")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Region> getRegionsByParametres(@QueryParam("parentId") int parentId,
                                               @QueryParam("extractionTs") Timestamp extractionts, @QueryParam("language") int language,
                                               @QueryParam("type") int type, @QueryParam("name") String name, @QueryParam("projectId") int projectId,
                                               @QueryParam("commitId") int commitId, @QueryParam("commitTs") Timestamp commitTs) {
        return db.getRegionsByParametresWithParentId(parentId, extractionts, language, type, name, projectId, commitId,
                commitTs);

    }

    @POST
    @Path("region")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Region postRegion(Region r) {
        System.out.println("xelo");
        return db.postRegion(r);
    }

}
