package com.smef.api;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InputParameter {
	private String order;
	private String type;

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "InputParameter [order=" + order + ", type=" + type + "]";
	}
}
