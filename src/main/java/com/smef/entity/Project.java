package com.smef.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Project {
	private int id;
	private long extractionTimestamp;
	private String url;
	private String name;
	private String commitID;
	private long commitTS;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCommitID() {
		return commitID;
	}
	public void setCommitID(String commitID) {
		this.commitID = commitID;
	}
	public long getCommitTS() {
		return commitTS;
	}
	public void setCommitTS(long commitTS) {
		this.commitTS = commitTS;
	}
	public long getExtractionTimestamp() {
		return extractionTimestamp;
	}
	public void setExtractionTimestamp(long extractionTimestamp) {
		this.extractionTimestamp = extractionTimestamp;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}