package com.smef.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MetricStore {
	private int id;
	private int regionId;
	private int metricCode;
	private double value;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRegionId() {
		return regionId;
	}
	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}
	public int getMetricCode() {
		return metricCode;
	}
	public void setMetricCode(int metricCode) {
		this.metricCode = metricCode;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "MetricStore [id=" + id + ", regionId=" + regionId + ", metricCode=" + metricCode + ", value=" + value
				+ "]";
	}
	
	
	
}
