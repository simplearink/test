package com.smef.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Smef {
	
	private Project project = new Project();
	
	private List<Region> regions = new ArrayList<Region>();
	
	//private double extractedTS;
	//private String projectName;
	//private String repUrl;
	
	//private int toolId;
	//private int revId;
	//private String revTS;
	
	
	
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public List<Region> getRegions() {
		return regions;
	}
	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}
	@Override
	public String toString() {
		return "Smef [project=" + project + ", regions=" + regions + "]";
	}
	
	/*public double getExtractedTS() {
		return extractedTS;
	}
	public void setExtractedTS(double extractedTS) {
		this.extractedTS = extractedTS;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getRepUrl() {
		return repUrl;
	}
	public void setRepUrl(String repUrl) {
		this.repUrl = repUrl;
	}
	public int getToolId() {
		return toolId;
	}
	public void setToolId(int toolId) {
		this.toolId = toolId;
	}
	public int getRevId() {
		return revId;
	}
	public void setRevId(int revId) {
		this.revId = revId;
	}
	public String getRevTS() {
		return revTS;
	}
	public void setRevTS(String revTS) {
		this.revTS = revTS;
	}*/
	
	

}
