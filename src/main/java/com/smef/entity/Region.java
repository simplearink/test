package com.smef.entity;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Region {
	
	private int id;
	private int parentId;
	private long exctractionTimeStamp;
	private int language;
	private int type;
	private String name;
	private int projectId;
	private int commitId;
	private long commitTs;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public long getExctractionTimeStamp() {
		return exctractionTimeStamp;
	}
	public void setExctractionTimeStamp(long exctractionTimeStamp) {
		this.exctractionTimeStamp = exctractionTimeStamp;
	}
	public int getLanguage() {
		return language;
	}
	public void setLanguage(int language) {
		this.language = language;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public int getCommitId() {
		return commitId;
	}
	public void setCommitId(int commitId) {
		this.commitId = commitId;
	}
	public long getCommitTs() {
		return commitTs;
	}
	public void setCommitTs(long commitTs) {
		this.commitTs = commitTs;
	}

		
	
	


}
